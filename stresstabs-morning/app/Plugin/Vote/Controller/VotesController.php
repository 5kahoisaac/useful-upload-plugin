<?php
App::uses('VoteAppController', 'Vote.Controller');
/**
 * Votes Controller
 *
 * @property Vote $Vote
*/
class VotesController extends VoteAppController {

	public $helpers = array('Csv');
	
	/**
	 * index method
	 *
	 * @return void
	 */
	public function index() {
		if ($this->request->isAjax()){
			$this->layout = null;
		}
		$this->Vote->recursive = 0;
		
		if ($this->request->isGet()){
			if (!empty($this->request->named['filter'])){
				$filter = array();
				$filter['Vote']['filter'] = $this->request->named['filter'];
				if (!empty($this->request->params['named']['page'])){
					$filter['Vote']['page'] = $this->request->named['page'];
				}else{
					$filter['Vote']['page'] = 1;
				}
				$this->request->data = am($this->request->data,$filter);
			}else{
				$filter = array();
				$filter['Vote'] = $this->Cookie->read('srcPassArg');
				if (!empty($filter['Vote'])){
					$this->request->data = am($this->request->data,$filter);
				}
			}
		}
		
		
		$passArg = array();
		$conditions = array();
		if (!empty($this->data['Vote']) && !empty($this->data['Vote']['filter'])){
			$condition = array(' title LIKE '  => '%'.trim($this->data['Vote']['filter']).'%');
			$passArg = $this->data['Vote'];
			array_push($conditions,$condition);
		}
		if (!empty($this->request->params['named']['page'])){
			$passArg['page'] = 1;
		}else{
			if (!empty($this->request->data['Vote']['page'])){
				$this->request->params['named']['page'] = $this->request->data['Vote']['page'];
			}
		}
		
		//$paginate = array('limit' => 2);
		$paginate = array();
		if ($this->Session->read('auth_user')['Group'][0]['id']!=1){
			$condition = array('owner_id'=>$this->Session->read('auth_user')['User']['id']);
			array_push($conditions,$condition);
		}
		if (!empty($conditions)){
			$paginate['conditions'] = $conditions;
		}
		
		//print_r($this->data);
		
		//$paginate['order']="Vote.ordering DESC";
		
		$this->paginate = $paginate;
		
		$this->set('passArg',$passArg);
		
		if (!empty($passArg)){
			$this->Cookie->write('srcPassArg',$passArg);
		}
		
		$this->set('votes', $this->paginate());		
		
	}

	/**
	 * view method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function view($id = null) {
		$this->Vote->id = $id;
		if (!$this->Vote->exists()) {
			throw new NotFoundException(__('Invalid vote'));
		}
		$this->set('vote', $this->Vote->read(null, $id));
	}

	/**
	 * add method
	 *
	 * @return void
	 */
	public function add() {
		$errors = array();
		if ($this->request->is('post')) {
			$this->Vote->create();
			
			$user = $this->Session->read('Auth');
			$this->request->data['Vote']['owner_id']=$user['User']['id'];
			
			if ($this->Vote->save($this->request->data)) {
				$this->Cookie->delete('srcPassArg');
				$this->redirect(array('action' => 'edit',$this->Vote->id));
			} else {
				$errors = $this->Vote->validationErrors;
			}
		}
		
		$this->set('errors',$errors);
		$this->set(compact('categories'));
	}

	/**
	 * edit method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function edit($id = null) {
		$errors = array();
		$this->Vote->id = $id;
		if (!$this->Vote->exists()) {
			throw new NotFoundException(__('Invalid vote'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Vote->save($this->request->data)) {
				$this->redirect(array('action' => 'index'));
			} else {
				$errors = $this->Vote->validationErrors;
			}
		} else {
			$vote = $this->Vote->read(null, $id);
			if ($vote['Vote']['owner_id']!=$this->Session->read('auth_user')['User']['id'] && $this->Session->read('auth_user')['Group'][0]['id']!=1)
				$this->redirect(array('action'=>'index')); 
			$this->request->data = am($this->request->data,$vote);
		}
		$this->Session->write("CurrentPID",$id);
		$this->set('errors',$errors);
		$this->set(compact('categories'));
	}

	/**
	 * delete method
	 *
	 * @throws MethodNotAllowedException
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function delete($id = null) {
		$this->autoRender = false;
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Vote->id = $id;
		if (!$this->Vote->exists()) {
			throw new NotFoundException(__('Invalid vote'));
		}
		if ($this->Vote->delete()) {
			//$this->Session->setFlash(__('Vote deleted'));
			//$this->redirect(array('action' => 'index'));
		}
		//$this->Session->setFlash(__('Vote was not deleted'));
		//$this->redirect(array('action' => 'index'));
	}

	public function download(){		
		$this->set('votes', $this->Vote->find('all',array()));		
		$this->layout = null;
		$this->autoLayout = false;
		Configure::write('debug', 0);
	}
}
