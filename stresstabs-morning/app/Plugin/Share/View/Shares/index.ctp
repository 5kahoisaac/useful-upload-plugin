<div class="container">
	<h2>
		<?php echo __('Share Manager'); ?>
	</h2>
	
	
	<div class="row-fluid show-grid">
		<div class="span12">
		<div class="pagination">
				<ul>
					<?php
					echo $this->Paginator->prev('&larr; ' . __('previous'),array('tag' => 'li','escape' => false));
					echo $this->Paginator->numbers(array('separator' => '','tag'=>'li'));
					echo $this->Paginator->next(__('next') . ' &rarr;', array('tag' => 'li','escape' => false));
					?>
				</ul>
			</div>
			<table class="table table-bordered table-hover list table-condensed table-striped">
				<thead>
					<tr>
						<th style="width: 30px;"><?php echo $this->Paginator->sort('id','ID'); ?>
						</th>
						<th style="text-align: center; width:230px;"><?php echo $this->Paginator->sort('name','Name'); ?>
						</th>
						<th style="text-align: center; width:80px;"><?php echo $this->Paginator->sort('email','Email'); ?>
						</th>
						<th style="text-align: center; width:80px;"><?php echo $this->Paginator->sort('fbid','FB id'); ?>
						</th>
						
					</tr>
				</thead>
				<?php
	foreach ($shares as $share): ?>
				<tr>
					<td><?php echo h($share['Share']['id']); ?>&nbsp;</td>
					<td><?php echo h($share['Share']['name']); ?>&nbsp;</td>
					<td><?php echo $share['Share']['email']; ?>&nbsp;</td>
					<td><?php echo $share['Share']['fbid']; ?>&nbsp;</td>
					<?php if ($this->Acl->check('Shares','view','Share') == true || $this->Acl->check('Shares','edit','Share') == true || $this->Acl->check('Shares','delete','Share') == true){?>
					
					<?php } ?>
				</tr>
				<?php endforeach; ?>
			</table>
			<p>
				<?php
				echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>
			</p>

			<div class="pagination">
				<ul>
					<?php
					echo $this->Paginator->prev('&larr; ' . __('previous'),array('tag' => 'li','escape' => false));
					echo $this->Paginator->numbers(array('separator' => '','tag'=>'li'));
					echo $this->Paginator->next(__('next') . ' &rarr;', array('tag' => 'li','escape' => false));
					?>
				</ul>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
function cancelSearch(){
	removeUserSearchCookie();
	window.location = '<?php echo Router::url(array('plugin' => 'share','controller' => 'shares','action' => 'index')); ?>';
}
function showAddSharePage() {
	window.location = "<?php echo Router::url(array('plugin' => 'share','controller' => 'shares','action' => 'add')); ?>";
}
function delShare(share_id, name) {
    $.sModal({
        image: '<?php echo $this->webroot; ?>img/icons/error.png',
        content: '<?php echo __('Are you sure you want to delete'); ?>  <b>' + name + '</b>?',
        animate: 'fadeDown',
        buttons: [{
            text: ' <?php echo __('Delete'); ?> ',
            addClass: 'btn-danger',
            click: function(id, data) {
                $.post('<?php echo Router::url(array('plugin' => 'share','controller' => 'shares','action' => 'delete')); ?>/' + share_id, {}, function(o) {
	                    $('#container').load('<?php echo Router::url(array('plugin' => 'share','controller' => 'shares','action' => 'index')); ?>', function() {
                        $.sModal('close', id);
                        $('#tab_user_manager').find('a').each(function(){
                    		$(this).click(function(){
                    			removeUserSearchCookie();
                    		});
                    	});
                    });
                }, 'json');
            }
        }, {
            text: ' <?php echo __('Cancel'); ?> ',
            click: function(id, data) {
                $.sModal('close', id);
            }
        }]
        });
}
$(document).ready(function() {
	$('.pagination > ul > li').each(function() {
		if ($(this).children('a').length <= 0) {
			var tmp = $(this).html();
			if ($(this).hasClass('prev')) {
				$(this).addClass('disabled');
			} else if ($(this).hasClass('next')) {
				$(this).addClass('disabled');
			} else {
				$(this).addClass('active');
			}
			$(this).html($('<a></a>').append(tmp));
		}
	});
});
</script>
