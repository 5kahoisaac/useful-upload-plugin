<div class="container">
	<h2>
		<?php echo __('Vote Manager'); ?>
	</h2>
	
	<?php echo $this->Form->create('Vote', array('action' => 'index','class'=>' form-signin form-horizontal')); ?>
	
	<?php echo $this->Form->end(); ?>
	<div class="row-fluid show-grid">
		<div class="span12">
		<div class="pagination">
				<ul>
					<?php
					echo $this->Paginator->prev('&larr; ' . __('previous'),array('tag' => 'li','escape' => false));
					echo $this->Paginator->numbers(array('separator' => '','tag'=>'li'));
					echo $this->Paginator->next(__('next') . ' &rarr;', array('tag' => 'li','escape' => false));
					?>
				</ul>
			</div>
			<table class="table table-bordered table-hover list table-condensed table-striped">
				<thead>
					<tr>
						<th style="width: 30px;"><?php echo $this->Paginator->sort('id','ID'); ?>
						</th>
						<th style="text-align: center; width:230px;"><?php echo $this->Paginator->sort('name','Name'); ?>
						</th>
						<th style="text-align: center; width:80px;"><?php echo $this->Paginator->sort('gender','Gender'); ?>
						</th>
						<th style="text-align: center; width:80px;"><?php echo $this->Paginator->sort('age','Age'); ?>
						</th>
						<th style="text-align: center; width:80px;"><?php echo $this->Paginator->sort('mobile','Mobile'); ?>
						</th>
						<th style="text-align: center; width:80px;"><?php echo $this->Paginator->sort('receipt_1','Receipt'); ?>
						</th>
						<th style="text-align: center; width:80px;"><?php echo $this->Paginator->sort('shop','Shop'); ?>
						</th>
						<th style="text-align: center; width:80px;"><?php echo $this->Paginator->sort('filename','File name'); ?>
						</th>
						<th style="text-align: center; width:80px;"><?php echo $this->Paginator->sort('round1','Round 1'); ?>
						</th>
						<th style="text-align: center; width:80px;"><?php echo $this->Paginator->sort('round2','Round 2'); ?>
						</th>
						<th style="text-align: center; width:80px;"><?php echo $this->Paginator->sort('product1','Product 1'); ?>
						</th>
						<th style="text-align: center; width:80px;"><?php echo $this->Paginator->sort('product2','Product 2'); ?>
						</th>
						<th style="text-align: center; width:80px;"><?php echo $this->Paginator->sort('product3','Product 3'); ?>
						</th>
						<th style="text-align: center; width:80px;"><?php echo $this->Paginator->sort('product4','Product 4'); ?>
						</th>
						<th style="text-align: center; width:80px;"><?php echo $this->Paginator->sort('created','Created'); ?>
						</th>
						<th style="text-align: center; width:80px;"><?php echo $this->Paginator->sort('modified','Modified'); ?>
						</th>
						
					</tr>
				</thead>
				<?php
	foreach ($votes as $vote): ?>
				<tr>
					<td><?php echo h($vote['Vote']['id']); ?>&nbsp;</td>
					<td><?php echo h($vote['Vote']['name']); ?>&nbsp;</td>
					<td><?php echo h($vote['Vote']['gender']); ?>&nbsp;</td>
					<td><?php echo $vote['Vote']['age']; ?>&nbsp;</td>
					<td><?php echo $vote['Vote']['mobile']; ?>&nbsp;</td>
					<td><?php echo $vote['Vote']['receipt_1'].$vote['Vote']['receipt_2']; ?>&nbsp;</td>
					<td><?php echo $vote['Vote']['shop']; ?>&nbsp;</td>
					<td><a href="<?php echo $this->webroot;?>files/<?php echo $vote['Vote']['filename']; ?>" target="_blank"><?php echo $vote['Vote']['filename']; ?></a>&nbsp;</td>
					<td><?php echo $vote['Vote']['round1']; ?>&nbsp;</td>
					<td><?php echo $vote['Vote']['round2']; ?>&nbsp;</td>
					<td><?php echo $vote['Vote']['product1']; ?>&nbsp;</td>
					<td><?php echo $vote['Vote']['product2']; ?>&nbsp;</td>
					<td><?php echo $vote['Vote']['product3']; ?>&nbsp;</td>
					<td><?php echo $vote['Vote']['product4']; ?>&nbsp;</td>
					<td><?php echo $vote['Vote']['created']; ?>&nbsp;</td>
					<td><?php echo $vote['Vote']['modified']; ?>&nbsp;</td>
					
					
				</tr>
				<?php endforeach; ?>
			</table>
			<p>
				<?php
				echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>
			</p>

			<div class="pagination">
				<ul>
					<?php
					echo $this->Paginator->prev('&larr; ' . __('previous'),array('tag' => 'li','escape' => false));
					echo $this->Paginator->numbers(array('separator' => '','tag'=>'li'));
					echo $this->Paginator->next(__('next') . ' &rarr;', array('tag' => 'li','escape' => false));
					?>
				</ul>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
function cancelSearch(){
	removeUserSearchCookie();
	window.location = '<?php echo Router::url(array('plugin' => 'vote','controller' => 'votes','action' => 'index')); ?>';
}
function showAddVotePage() {
	window.location = "<?php echo Router::url(array('plugin' => 'vote','controller' => 'votes','action' => 'add')); ?>";
}
function delVote(vote_id, name) {
    $.sModal({
        image: '<?php echo $this->webroot; ?>img/icons/error.png',
        content: '<?php echo __('Are you sure you want to delete'); ?>  <b>' + name + '</b>?',
        animate: 'fadeDown',
        buttons: [{
            text: ' <?php echo __('Delete'); ?> ',
            addClass: 'btn-danger',
            click: function(id, data) {
                $.post('<?php echo Router::url(array('plugin' => 'vote','controller' => 'votes','action' => 'delete')); ?>/' + vote_id, {}, function(o) {
	                    $('#container').load('<?php echo Router::url(array('plugin' => 'vote','controller' => 'votes','action' => 'index')); ?>', function() {
                        $.sModal('close', id);
                        $('#tab_user_manager').find('a').each(function(){
                    		$(this).click(function(){
                    			removeUserSearchCookie();
                    		});
                    	});
                    });
                }, 'json');
            }
        }, {
            text: ' <?php echo __('Cancel'); ?> ',
            click: function(id, data) {
                $.sModal('close', id);
            }
        }]
        });
}
$(document).ready(function() {
	$('.pagination > ul > li').each(function() {
		if ($(this).children('a').length <= 0) {
			var tmp = $(this).html();
			if ($(this).hasClass('prev')) {
				$(this).addClass('disabled');
			} else if ($(this).hasClass('next')) {
				$(this).addClass('disabled');
			} else {
				$(this).addClass('active');
			}
			$(this).html($('<a></a>').append(tmp));
		}
	});
});
</script>
